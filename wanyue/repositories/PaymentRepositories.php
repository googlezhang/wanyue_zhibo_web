<?php

namespace wanyue\repositories;

use app\models\store\StoreOrder;
use app\models\user\UserRecharge;
use app\models\charge\Charge;							 

/**
 * Class PaymentRepositories
 * @package wanyue\repositories
 */
class PaymentRepositories
{

    /**
     * 公众号下单成功之后
     * @param $order
     * @param $prepay_id
     */
    public static function wechatPaymentPrepare($order, $prepay_id)
    {

    }

    /**
     * 小程序下单成功之后
     * @param $order
     * @param $prepay_id
     */
    public static function wechatPaymentPrepareProgram($order, $prepay_id)
    {

    }

    /**
     * 微信APP下单成功之后
     * @param $order
     * @param $prepay_id
     */
    public static function wechatPaymentPrepareApp($order, $prepay_id)
    {

    }

    /**
     * 使用余额支付订单时
     * @param $userInfo
     * @param $orderInfo
     */
    public static function yuePayProduct($userInfo, $orderInfo)
    {


    }

    /**
     * 订单支付成功之后
     * @param string|null $order_id 订单id
     * @return bool
     */
    public static function wechatProduct(string $order_id = null)
    {
        try {
            if (StoreOrder::be(['order_id' => $order_id, 'paid' => 1])) return true;
            return StoreOrder::paySuccess($order_id);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 充值成功后
     * @param string|null $order_id 订单id
     * @return bool
     */
    public static function wechatUserRecharge(string $order_id = null)
    {
        try {
            if (UserRecharge::be(['order_id' => $order_id, 'paid' => 1])) return true;
            return UserRecharge::rechargeSuccess($order_id);
        } catch (\Exception $e) {
            return false;
        }
    }
    /**
     * 充值成功后
     * @param string|null $order_id 订单id
     * @return bool
     */
    public static function wechatCoincharge(string $order_id = null)
    {
	//	file_put_contents('chargelog/'.date('y-md').'.txt','进入方法时间'.date('Y-m-d h:i:s')."\r\n",FILE_APPEND);
        try {
            if (Charge::be(['orderno' => $order_id, 'status' => 1])) return true;
			file_put_contents('chargelog/'.date('y-md').'.txt','进入方法时间1'.date('Y-m-d h:i:s')."\r\n",FILE_APPEND);
            return Charge::rechargeSuccess($order_id);
        } catch (\Exception $e) {
            return false;
        }
    }		   
}