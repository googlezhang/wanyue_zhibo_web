<?php
// +----------------------------------------------------------------------
// |万岳科技开源系统 [山东万岳信息科技有限公司]
// +----------------------------------------------------------------------
// | Copyright (c) 2020~2022 https://www.sdwanyue.com All rights reserved.
// +----------------------------------------------------------------------
// | 万岳科技相关开源系统，需标注"代码来源于万岳科技开源项目"后即可免费自用运营，前端运营时展示的内容不得使用万岳科技相关信息
// +----------------------------------------------------------------------
// | Author: 万岳科技开源官方 < wanyuekj2020@163.com >
// +----------------------------------------------------------------------
namespace wanyue\repositories;

/**
 * 微信公众号操作类
 * Class MessageRepositories
 * @package wanyue\repositories
 */
class MessageRepositories
{
    /**
     * 位置 事件
     * @param $message
     * @return string
     */
   public static function wechatEventLocation($message)
   {
      // return 'location';
   }

    /**
     * 跳转URL  事件
     * @param $message
     * @return string
     */
   public static function wechatEventView($message)
   {
       //return 'view';
   }

    /**
     * 图片 消息
     * @param $message
     * @return string
     */
   public static function wechatMessageImage($message)
   {
       //return 'image';
   }

    /**
     * 语音 消息
     * @param $message
     * @return string
     */
   public static function wechatMessageVoice($message)
   {
       //return 'voice';
   }

    /**
     * 视频 消息
     * @param $message
     * @return string
     */
   public static function wechatMessageVideo($message)
   {
       //return 'video';
   }

    /**
     * 位置  消息
     */
   public static function wechatMessageLocation($message)
   {
       //eturn 'location';
   }

    /**
     * 链接   消息
     * @param $message
     * @return string
     */
   public static function wechatMessageLink($message)
   {
       //return 'link';
   }

    /**
     * 其它消息  消息
     */
   public static function wechatMessageOther($message)
   {
       //return 'other';
   }
}