<?php
/**
 * Created by PhpStorm.
 * User: xurongyao <763569752@qq.com>
 * Date: 2019/11/23 3:47 PM
 */

namespace wanyue\services\template;

use wanyue\basic\BaseManager;
use think\facade\Config;

/**
 * Class Template
 * @package wanyue\services\template
 * @mixin \wanyue\services\template\storage\Wechat
 */
class Template extends BaseManager
{

    /**
     * 空间名
     * @var string
     */
    protected $namespace = '\\wanyue\\services\\template\\storage\\';

    /**
     * 设置默认
     * @return mixed
     */
    protected function getDefaultDriver()
    {
        return Config::get('template.default', 'wechat');
    }
}