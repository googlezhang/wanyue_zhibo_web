<?php


namespace wanyue\interfaces;


interface ListenerInterface
{
    public function handle($event): void;
}