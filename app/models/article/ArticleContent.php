<?php

namespace app\models\article;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;


/**
 * 文章详情
 * Class ArticleCategory
 * @package app\models\article
 */
class ArticleContent extends BaseModel
{
    use ModelTrait;
}