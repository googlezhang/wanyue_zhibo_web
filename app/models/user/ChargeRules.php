<?php

namespace app\models\user;


use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;

/**
 * TODO 充值规则
 */
class ChargeRules extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'charge_rules';

    use ModelTrait;

    /**
     * 获取列表
     * @param $uids
     * @return int
     */
    public static function getlist()
    {
        //$model = new self();
        $list = self::order('list_order asc')->select()->toArray();
		return $list;
    }
    /**
     * 获取列表
     * @param $uids
     * @return int
     */
    public static function getlists()
    {
        //$model = new self();
        $list = self::order('list_order asc')->select()->toArray();
        $count = self::count();
		$data=$list;
        return compact('count', 'data');		
    }
    public static function getInfo($id)
    {
        $data = self::where('id',$id)->find();
        if($data){
            $data=$data->toArray();
        }
        return $data;
    }	
    public static function delid($id)
    {
        return self::del($id);
    }	
}