<?php

namespace app\models\record;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * TODO 礼物Model
 * Class Gift
 * @package app\models\user
 */
class Coinrecord extends BaseModel
{
    use ModelTrait;

    protected $pk = 'id';

    protected $name = 'coinrecord';

}