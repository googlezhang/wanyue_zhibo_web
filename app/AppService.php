<?php

namespace app;

use wanyue\services\SystemConfigService;
use wanyue\services\GroupDataService;
use wanyue\utils\Json;
use think\facade\Db;
use think\Service;

class AppService extends Service
{

    public $bind = [
        'json' => Json::class,
        'sysConfig' => SystemConfigService::class,
        'sysGroupData' => GroupDataService::class
    ];

    public function boot()
    {

    }
}
