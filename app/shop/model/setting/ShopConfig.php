<?php

namespace app\shop\model\setting;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * 店铺配置
 * Class SystemMenus
 * @package app\admin\model\system
 */
class ShopConfig extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'uid';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'shop_config';

    use ModelTrait;


    /**
     * 获取配置
     * @return array
     */
    public static function getC($uid = 0)
    {
        $config=[];
        $config_uid=self::where('uid',$uid)->find();
        if($config_uid){
            $config=json_decode($config_uid['value'],true);
        }
        return $config;
    }

    /**
     * 设置
     * @return array
     */
    public static function setC($uid = 0,$config)
    {
        $data=[
            'value'=>json_encode($config),
        ];
        $isexist=self::where('uid',$uid)->find();
        if($isexist){
            self::edit($data,$uid,'uid');
        }else{
            $data['uid']=$uid;
            self::create($data);
        }
        return 1;
    }
}