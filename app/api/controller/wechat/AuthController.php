<?php


namespace app\api\controller\wechat;


use app\models\user\WechatUser;
use app\Request;
use wanyue\services\CacheService;
use wanyue\services\WechatAppService;
use wanyue\services\UtilService;
use app\models\user\UserToken;
use wanyue\services\SystemConfigService;
use app\models\user\User;
use app\models\live\Live;
use think\facade\Cache;

/**
 * 小程序相关
 * Class AuthController
 * @package app\api\controller\wechat
 */
class AuthController
{

    /**
     * APP三方登录
     * @param Request $request
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function third_auth(Request $request)
    {

        list($type,$openid,$unionid,$avatar,$nickname,$sign,$gender,$language,$city,$province,$country,$source) = UtilService::postMore([
            ['type', ''],
            ['openid', ''],
            ['unionid', ''],
            ['avatar', ''],
            ['nickname', ''],
            ['sign', ''],
            [['gender','d'], 0],
            ['language', ''],
            ['city', ''],
            ['province', ''],
            ['country', ''],
            ['source', 0],
        ],$request, true);

        if ($type=='' || $openid=='' || $sign=='') return app('json')->fail('参数错误');

        $check_data=[
            'openid'=>$openid,
            'type'=>$type,
        ];
        if(!checkSign($check_data,$sign)) return app('json')->fail('签名错误');
        $avatar=urldecode($avatar);
        $data=[
            'nickName'=>$nickname,
            'gender'=>$gender,
            'language'=>$language,
            'city'=>$city,
            'province'=>$province,
            'country'=>$country,
            'avatarUrl'=>$avatar,
            'openId'=>$openid,
            'unionId'=>$unionid,
            'routine_openid'=>'',
            'login_type'=>$type,
            'session_key'=>'',
            'code'=>'',
            'spid'=>0,
        ];

        if($source==1){
            $data['user_type'] = 'Android';
        }
        if($source==2){
            $data['user_type'] = 'IOS';
        }

        $uid = WechatUser::routineOauth($data);

        $userInfo = User::where('uid', $uid)->find();

        if(!$userInfo->status) return app('json')->fail('该账号已被禁用');

        //if($source!=0 && $userInfo->isshop!=1) return app('json')->fail('该账号暂未认证店铺\n无法登录');

        if(Live::islive($userInfo->uid)) return app('json')->fail('该账号正在直播中，无法登陆');

        if ($userInfo->login_type == 'h5' && ($h5UserInfo = User::where(['account' => $userInfo->phone, 'phone' => $userInfo->phone, 'user_type' => 'h5'])->find()))
            $token = UserToken::createToken($userInfo, 'routine');
        else
            $token = UserToken::createToken($userInfo, 'routine');
        if ($token) {
            event('UserLogin', [$userInfo, $token]);
            return app('json')->successful('登陆成功！', [
                'token' => $token->token
            ]);
        } else
            return app('json')->fail('获取用户访问token失败!');
    }

    /**
     * APP支付回调
     */
    public function notify_app()
    {
        WechatAppService::handleNotify();
    }

}