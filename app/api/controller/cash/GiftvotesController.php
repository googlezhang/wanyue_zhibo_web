<?php
namespace app\api\controller\cash;

use app\models\user\User;
use app\models\cash\Giftvotes;
use app\Request;
use wanyue\services\UtilService;

/**
 * 映票提现
 */
class GiftvotesController
{
    /**
     * 统计
     */
    public function info(Request $request)
    {

        $uid=$request->uid();

        $userinfo=User::getUserInfo($uid,'votes,votestotal');

        $votes=$userinfo['votes'];
        $votestotal=$userinfo['votestotal'];
        $votes_ing=Giftvotes::cashmoney($uid);
		$votes_per = sys_config('giftcash_per');
        return app('json')->successful(compact('votes','votes_ing','votestotal','votes_per'));
    }

    /**
     * 列表
     */
    public function lst(Request $request)
    {
        list( $page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        $uid=$request->uid();

        $list=Giftvotes::getList($uid,$page,$limit);

        return app('json')->successful($list);
    }

    //提现
    public function cash(Request $request)
    {
        list($money,$accountid) = UtilService::postMore([
            ['money', 0],
            [['accountid', 'd'], 0],
        ], $request, true);

        if($money<=0 || $accountid<1) return app('json')->fail('参数错误');
        $uid=$request->uid();

        $res=Giftvotes::cash($uid,$money,$accountid);

        if (!$res) return app('json')->fail(Giftvotes::getErrorInfo());

        return app('json')->successful('提交成功');
    }

}