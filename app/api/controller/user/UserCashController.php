<?php
namespace app\api\controller\user;

use app\models\user\User;
use app\models\user\UserVote;
use app\Request;
use wanyue\services\UtilService;

/**
 * 会员提现类
 */
class UserCashController
{

    /**
     * 钻石收益提现
     */
    public function votelist(Request $request)
    {
        list( $page, $limit) = UtilService::getMore([
            [['page', 'd'], 1],
            [['limit', 'd'], 20]
        ], $request, true);

        $uid=$request->uid();

        $list=UserVote::getList($uid,$page,$limit);

        return app('json')->successful($list);
    }

    //钻石提现
    public function cashvotes(Request $request)
    {
        list($votes,$accountid) = UtilService::postMore([
            ['votes', 0],
            [['accountid', 'd'], 0],
        ], $request, true);

        if($votes<=0 || $accountid<1) return app('json')->fail('参数错误');
        $uid=$request->uid();

        $res=UserVote::cash($uid,$votes,$accountid);

        if (!$res) return app('json')->fail(UserVote::getErrorInfo());

        return app('json')->successful('提交成功');
    }


}