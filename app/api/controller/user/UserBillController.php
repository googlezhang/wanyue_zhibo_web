<?php

namespace app\api\controller\user;


use app\models\user\UserBill;
use app\Request;
use wanyue\services\UtilService;

/**
 * 账单类
 * Class UserBillController
 * @package app\api\controller\user
 */
class UserBillController
{

    /**
     * 积分记录
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function integral_list(Request $request)
    {
        list($page, $limit) = UtilService::getMore([
            [['page', 'd'], 0], [['limit', 'd'], 0]
        ], $request, true);
        return app('json')->successful(UserBill::userBillList($request->uid(), $page, $limit));

    }
}