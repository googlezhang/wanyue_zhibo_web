<?php

namespace app\api\controller\live;

use app\models\live\LiveKick;
use app\models\live\LiveManager;
use app\models\live\LiveShut;
use app\models\live\Shutlist;
use app\models\live\Live;
use app\models\user\User;
use app\models\user\UserAttent;
use app\Request;
use wanyue\services\UtilService;
use app\Redis;
class LiveManageController
{
    /**
     * 弹窗
     */
    public function getpop(Request $request)
    {
        list($touid, $liveuid) = UtilService::getMore([
            [['touid', 'd'], 0],
            [['liveuid', 'd'], 0],
        ], $request, true);


        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();
        $userinfo=User::getUserInfoByRedis($touid);
        if(!$userinfo) return app('json')->fail('用户信息不存在');

        $follows=UserAttent::getFollsNums($touid);
        $fans=UserAttent::getFansNums($touid);

        $userinfo['follows']=NumberFormat($follows);
        $userinfo['fans']=NumberFormat($fans);



		//判断权限
		$uid_admin=LiveManager::checkmanager($uid,$liveuid);
		$touid_admin=LiveManager::checkmanager($touid,$liveuid);
		$action=0;
		if($uid_admin==40 && $touid_admin==30){
			$action=40;
		}else if($uid_admin==50 && $touid_admin==30){
			$action=501;
		}else if($uid_admin==50 && $touid_admin==40){
			$action=502;
		}else if($uid_admin==60 && $touid_admin<50){
			$action=40;
		}else if($uid_admin==60 && $touid_admin==50){
			$action=60;
		}else if($touid_admin==60){
			$action=70;
		}else{
			$action=30;
		}


        
       /*  if($uid==$liveuid){
            $action=50;
        } */
        $isshut=0;
        if($action>0){
            $isshut=LiveShut::isShut($touid,$liveuid);
        }

        $userinfo['action']=$action;
        $userinfo['isshut']=$isshut;
        return app('json')->successful($userinfo);
    }

    //踢人
    public function kick(Request $request)
    {
        list($touid, $liveuid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();

        $res = LiveKick::kick($uid,$touid,$liveuid);
        if(!$res) return app('json')->fail(LiveKick::getErrorInfo());

        return app('json')->successful('操作成功');
    }
    /**
     * 取消踢人
     */
    public function unkick(Request $request)
    {

        list($touid, $liveuid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();

        $res = LiveKick::delKickuser($uid, $touid,$liveuid);
        if (!$res) return app('json')->fail(LiveKick::getErrorInfo());

        return app('json')->successful('操作成功');
    }	

    /**
     * 禁言时长列表
     */
    public function shutlist(Request $request)
    {


        $uid=$request->uid();

        $list = Shutlist::select()->toArray();
		

        return app('json')->successful($list);
    }

    /**
     * 禁言
     */
    public function shut(Request $request)
    {

        list($touid, $liveuid,$shutid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
            [['shutid','d'],0],
        //    [['uid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1|| $shutid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();

        $res = LiveShut::shut($uid, $touid,$liveuid,$shutid);
        if (!$res) return app('json')->fail(LiveShut::getErrorInfo());
		

		
        return app('json')->successful('操作成功');
    }

    /**
     * 取消禁言
     */
    public function unshut(Request $request)
    {

        list($touid, $liveuid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();

        $res = LiveShut::delShut($uid, $touid,$liveuid);
        if (!$res) return app('json')->fail(LiveShut::getErrorInfo());

        return app('json')->successful('操作成功');
    }

    /**
     * 设置管理员
     */
    public function setmanager(Request $request)
    {

        list($touid, $liveuid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();
		if($liveuid!=$uid){
			return app('json')->fail('参数错误');
		}

		$user_management=LiveManager::checkmanager($touid,$liveuid);
		
		
		if($user_management>30){
			return app('json')->fail('操作失败');
		}
		$data=[
			'uid'=>$touid,
			'liveuid'=>$liveuid
			
		];
		
        $res = LiveManager::insert($data);
        if (!$res) return app('json')->fail(LiveManager::getErrorInfo());

        return app('json')->successful('操作成功');
    }

    /**
     * 取消管理员
     */
    public function delmanager(Request $request)
    {

        list($touid, $liveuid) = UtilService::postMore([
            [['touid','d'],0],
            [['liveuid','d'],0],
        ], $request, true);

        if($touid<1 || $liveuid<1) return app('json')->fail('参数错误');

        $uid=$request->uid();
		if($liveuid!=$uid){
			return app('json')->fail('参数错误');
		}
		$user_management=LiveManager::checkmanager($touid,$liveuid);
		if($user_management!=40){
			return app('json')->fail('操作失败');
		}
		$data=[
			'uid'=>$touid,
			'liveuid'=>$liveuid
		];
		
        $res = LiveManager::delmanager($liveuid,$touid);
        if (!$res) return app('json')->fail(LiveManager::getErrorInfo());

        return app('json')->successful('操作成功');
    }

    /**
     * 被禁言列表
     */
    public function usershutlist(Request $request)
    {

        list($p,$liveuid) = UtilService::postMore([
            [['p','d'],1],
            [['liveuid','d'],0],
        ], $request, true);
		if($p<1) return app('json')->fail('参数错误');
        $uid=$request->uid();
		$check= LiveManager::checkmanager($uid,$liveuid);
		if($check !=40 && $check !=50 && $check !=60){
			return app('json')->fail('您没有权限'.$check);
		}
        $list = LiveShut::getlist($liveuid,$p);
        return app('json')->successful($list);
    }
    /**
     * 被踢出列表
     */
    public function userkicklist(Request $request)
    {

        list($p,$liveuid) = UtilService::postMore([
            [['p','d'],1],
			[['liveuid','d'],0],
        ], $request, true);
		if($p<1) return app('json')->fail('参数错误');
        $uid=$request->uid();
		$check= LiveManager::checkmanager($uid,$liveuid);
		if($check !=40 && $check !=50 && $check !=60 ){
			return app('json')->fail('您没有权限');
		}
        $list = LiveKick::getlist($liveuid,$p);
		

        return app('json')->successful($list);
    }	
    /**
     * 管理房间列表
     */
    public function managelist(Request $request)
    {
        list($p) = UtilService::postMore([
            [['p','d'],1],
        ], $request, true);
		if($p<1) return app('json')->fail('参数错误');
        $uid=$request->uid();
        $list = LiveManager::managelist($uid,$p);
        return app('json')->successful($list);
    }	
    /**
     * 管理员列表
     */
    public function managerlist(Request $request)
    {
        list($p) = UtilService::postMore([
            [['p','d'],1],
        ], $request, true);
		if($p<1) return app('json')->fail('参数错误');
        $uid=$request->uid();
        $list = LiveManager::managerlist($uid,$p);
        $count = LiveManager::managercount($uid);
        return app('json')->successful(compact('count', 'list'));
    }		
    /**
     * 超管关播 
     */
    public function supershut(Request $request)
    {
        list($liveuid,$stream) = UtilService::postMore([
            [['liveuid','d'],0],
            ['stream',''],
        ], $request, true);

		if($liveuid<1) return app('json')->fail('参数错误');
        $uid=$request->uid();
		$check= LiveManager::checkmanager($uid,$liveuid);
		if($check !=60){
			return app('json')->fail('您没有权限');
		}
        $stream_a=explode('_',$stream);
        $uid_r = isset($stream_a[0])? $stream_a[0] : '0';
        $showid = isset($stream_a[1])? $stream_a[1] : '0';
		if($uid_r!=$liveuid) return app('json')->fail('参数不一致!');
        
        $res = Live::stopLive($liveuid,$showid);
        if (!$res) return app('json')->fail(Live::getErrorInfo());
        return app('json')->successful('关播成功');
    }	
}