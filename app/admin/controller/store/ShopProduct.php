<?php

namespace app\admin\controller\store;

use app\admin\controller\AuthController;
use app\admin\model\store\{
    StoreCategory as CategoryModel,
    StoreProduct as ProductModel
};
use app\models\user\User as UserModel;
use wanyue\services\{
    JsonService, UtilService as Util, JsonService as Json, FormBuilder as Form
};
use wanyue\traits\CurdControllerTrait;
use think\facade\Route as Url;
use app\admin\model\system\{
    SystemAttachment, ShippingTemplates
};
use function Sodium\compare;


/**
 * 产品管理
 * Class StoreProduct
 * @package app\admin\controller\store
 */
class ShopProduct extends AuthController
{

    use CurdControllerTrait;

    protected $bindModel = ProductModel::class;

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $type = $this->request->param('type', 8);
        //获取分类
        $this->assign('cate', CategoryModel::getTierList(null, 1));
        $this->assign(compact('type'));
        return $this->fetch();
    }

    /**
     * 异步查找产品
     *
     * @return json
     */
    public function product_ist()
    {
        $where = Util::getMore([
            ['page', 1],
            ['limit', 20],
            ['store_name', ''],
            ['cate_id', ''],
            ['excel', 0],
            ['mer_id', -1],
            ['order', ''],
            [['type', 'd'], $this->request->param('type/d')]
        ]);

        if($where['mer_id']==='0') $where['mer_id']='-2';
        if($where['mer_id']=='') $where['mer_id']='-1';

        $info=ProductModel::ProductList($where);
        $count=$info['count'];
        $data=$info['data'];
        foreach ($data as $k=>$v){
            $nickname='已删除';
            $userinfo=UserModel::getUserInfo($v['mer_id'],'nickname');
            if($userinfo){
                $nickname=$userinfo['nickname'];
            }
            $nickname.=' ('.$v['mer_id'].')';
            $v['nickname']=$nickname;
            $data[$k]=$v;
        }
        return Json::successlayui(compact('count', 'data'));
    }

    /**
     * 设置单个产品上架|下架
     *
     * @return json
     */
    public function set_show($is_show = '', $id = '')
    {
        ($is_show == '' || $id == '') && Json::fail('缺少参数');
        $is_off=1;
        if($is_show==1){
            $is_off=0;
        }
        $res = ProductModel::where(['id' => $id])->update(['is_show' => (int)$is_show,'is_off' => (int)$is_off]);
        if ($res) {
            return Json::successful($is_show == 1 ? '上架成功' : '下架成功');
        } else {
            return Json::fail($is_show == 1 ? '上架失败' : '下架失败');
        }
    }


    /**
     * 设置批量产品上架
     *
     * @return json
     */
    public function product_show()
    {
        $post = Util::postMore([
            ['ids', []]
        ]);
        if (empty($post['ids'])) {
            return Json::fail('请选择需要上架的产品');
        } else {
            $res = ProductModel::where('id', 'in', $post['ids'])->update(['is_show' => 1]);
            if ($res)
                return Json::successful('上架成功');
            else
                return Json::fail('上架失败');
        }
    }

}
