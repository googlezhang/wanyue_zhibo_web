<?php
/**
 *
 * @author: xaboy<365615158@qq.com>
 * @day: 2017/11/11
 */

namespace app\admin\model\order;

use app\models\user\User as UserModel;
use wanyue\basic\BaseModel;
use wanyue\traits\ModelTrait;
use app\admin\model\wechat\WechatUser;
use app\admin\model\user\UserBill;
use wanyue\services\PHPExcelService;

/**
 * 订单管理Model
 * Class StoreOrder
 * @package app\admin\model\store
 */
class ShopOrder extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'store_order';

    use ModelTrait;

    public static function orderCount()
    {
        $data['ys'] = self::statusByWhere(9, new self())->where(['is_system_del' => 0])->count();
        $data['wz'] = self::statusByWhere(0, new self())->where(['is_system_del' => 0])->count();
        $data['wf'] = self::statusByWhere(1, new self())->where(['is_system_del' => 0, 'shipping_type' => 1])->count();
        $data['ds'] = self::statusByWhere(2, new self())->where(['is_system_del' => 0, 'shipping_type' => 1])->count();
        $data['dp'] = self::statusByWhere(3, new self())->where(['is_system_del' => 0])->count();
        $data['jy'] = self::statusByWhere(4, new self())->where(['is_system_del' => 0])->count();
        $data['tk'] = self::statusByWhere(-1, new self())->where(['is_system_del' => 0])->count();
        $data['yt'] = self::statusByWhere(-2, new self())->where(['is_system_del' => 0])->count();
        $data['del'] = self::statusByWhere(-4, new self())->where(['is_system_del' => 0])->count();
        $data['write_off'] = self::statusByWhere(5, new self())->where(['is_system_del' => 0])->count();
        $data['general'] = self::where('mer_id',0)->where(['is_system_del' => 0])->count();
        return $data;
    }

    public static function OrderList($where)
    {
        $model = self::getOrderWhere($where, self::alias('a')
            ->join('user r', 'r.uid=a.uid', 'LEFT'), 'a.', 'r')
            ->field('a.*,r.nickname,r.phone');
        if ($where['order'] != '') {
            $model = $model->order(self::setOrder($where['order']));
        } else {
            $model = $model->order('a.id desc');
        }
        if (isset($where['excel']) && $where['excel'] == 1) {
            $data = ($data = $model->select()) && count($data) ? $data->toArray() : [];
        } else {
            $data = ($data = $model->page((int)$where['page'], (int)$where['limit'])->select()) && count($data) ? $data->toArray() : [];
        }

        foreach ($data as &$item) {
            $_info = StoreOrderCartInfo::where('oid', $item['id'])->field('cart_info')->select();
            $_info = count($_info) ? $_info->toArray() : [];
            foreach ($_info as $k => $v) {
                $cart_info = json_decode($v['cart_info'], true);
                if (!isset($cart_info['productInfo'])) $cart_info['productInfo'] = [];
                $_info[$k]['cart_info'] = $cart_info;
                unset($cart_info);
            }
            $item['_info'] = $_info;
            $item['add_time'] = $item['add_time'] ? date('Y-m-d H:i:s', $item['add_time']) : '';
            $item['back_integral'] = $item['back_integral'] ?: 0;

                if ($item['shipping_type'] == 1) {
                    $item['pink_name'] = '[普通订单]';
                    $item['color'] = '#895612';
                } else if ($item['shipping_type'] == 2) {
                    $item['pink_name'] = '[核销订单]';
                    $item['color'] = '#8956E8';
                }

            if ($item['paid'] == 1) {
                switch ($item['pay_type']) {
                    case 'weixin':
                        $item['pay_type_name'] = '微信支付';
                        break;
                    case 'yue':
                        $item['pay_type_name'] = '余额支付';
                        break;
                    case 'offline':
                        $item['pay_type_name'] = '线下支付';
                        break;
                    default:
                        $item['pay_type_name'] = '其他支付';
                        break;
                }
            } else {
                switch ($item['pay_type']) {
                    default:
                        $item['pay_type_name'] = '未支付';
                        break;
                    case 'offline':
                        $item['pay_type_name'] = '线下支付';
                        $item['pay_type_info'] = 1;
                        break;
                }
            }
            if ($item['paid'] == 0 && $item['status'] == 0) {
                $item['status_name'] = '未支付';
            } else if ($item['paid'] == 1 && $item['status'] == 0 && $item['shipping_type'] == 1 && $item['refund_status'] == 0) {
                $item['status_name'] = '未发货';
            } else if ($item['paid'] == 1 && $item['status'] == 0 && $item['shipping_type'] == 2 && $item['refund_status'] == 0) {
                $item['status_name'] = '未核销';
            } else if ($item['paid'] == 1 && $item['status'] == 1 && $item['shipping_type'] == 1 && $item['refund_status'] == 0) {
                $item['status_name'] = '待收货';
            } else if ($item['paid'] == 1 && $item['status'] == 1 && $item['shipping_type'] == 2 && $item['refund_status'] == 0) {
                $item['status_name'] = '未核销';
            } else if ($item['paid'] == 1 && $item['status'] == 2 && $item['refund_status'] == 0) {
                $item['status_name'] = '待评价';
            } else if ($item['paid'] == 1 && $item['status'] == 3 && $item['refund_status'] == 0) {
                $item['status_name'] = '已完成';
            } else if ($item['paid'] == 1 && $item['refund_status'] == 1) {
                $refundReasonTime = date('Y-m-d H:i', $item['refund_reason_time']);
                $refundReasonWapImg = json_decode($item['refund_reason_wap_img'], true);
                $refundReasonWapImg = $refundReasonWapImg ? $refundReasonWapImg : [];
                $img = '';
                if (count($refundReasonWapImg)) {
                    foreach ($refundReasonWapImg as $itemImg) {
                        if (strlen(trim($itemImg)))
                            $img .= '<img style="height:50px;" src="' . $itemImg . '" />';
                    }
                }
                if (!strlen(trim($img))) $img = '无';
                if (isset($where['excel']) && $where['excel'] == 1) {
                    $refundImageStr = implode(',', $refundReasonWapImg);
                    $item['status_name'] = <<<TEXT
退款原因:{$item['refund_reason_wap']} 
备注说明：{$item['refund_reason_wap_explain']}
退款时间：{$refundReasonTime}
凭证连接：{$refundImageStr}
TEXT;
                    unset($refundImageStr);
                } else {
                    $item['status_name'] = <<<HTML
<b style="color:#f124c7">申请退款</b><br/>
<span>退款原因：{$item['refund_reason_wap']}</span><br/>
<span>备注说明：{$item['refund_reason_wap_explain']}</span><br/>
<span>退款时间：{$refundReasonTime}</span><br/>
<span>退款凭证：{$img}</span>
HTML;
                }
            } else if ($item['paid'] == 1 && $item['refund_status'] == 2) {
                $item['status_name'] = '已退款';
            }
            if ($item['paid'] == 1 && $item['status'] == 0 && $item['shipping_type'] == 2) {
                $item['_status'] = 0;
            } else if ($item['paid'] == 0 && $item['status'] == 0 && $item['refund_status'] == 0) {
                $item['_status'] = 1;
            } else if ($item['paid'] == 1 && $item['status'] == 0 && $item['refund_status'] == 0) {
                $item['_status'] = 2;
            } else if ($item['paid'] == 1 && $item['refund_status'] == 1) {
                $item['_status'] = 3;
            } else if ($item['paid'] == 1 && $item['status'] == 1 && $item['refund_status'] == 0) {
                $item['_status'] = 4;
            } else if ($item['paid'] == 1 && $item['status'] == 2 && $item['refund_status'] == 0) {
                $item['_status'] = 5;
            } else if ($item['paid'] == 1 && $item['status'] == 3 && $item['refund_status'] == 0) {
                $item['_status'] = 6;
            } else if ($item['paid'] == 1 && $item['refund_status'] == 2) {
                $item['_status'] = 7;
            }

            /* 店铺信息 */
            $shopname='已删除';
            $userinfo=UserModel::getUserInfo($item['mer_id'],'nickname');
            if($userinfo){
                $shopname=$userinfo['nickname'];
            }
            $shopname.=' ('.$item['mer_id'].')';
            $item['shopname']=$shopname;

        }



        if (isset($where['excel']) && $where['excel'] == 1) {
            self::SaveExcel($data);
        }
        $count = self::getOrderWhere($where, self::alias('a')->join('user r', 'r.uid=a.uid', 'LEFT'), 'a.', 'r')->count();
        return compact('count', 'data');
    }

    /*
     * 保存并下载excel
     * $list array
     * return
     */
    public static function SaveExcel($list)
    {
        $export = [];
        foreach ($list as $index => $item) {
            $_info = StoreOrderCartInfo::where('oid', $item['id'])->column('cart_info');
            $goodsName = [];
            foreach ($_info as $k => $v) {
                $v = json_decode($v, true);
                $suk = '';
                if (isset($v['productInfo']['attrInfo'])) {
                    if (isset($v['productInfo']['attrInfo']['suk'])) {
                        $suk = '(' . $v['productInfo']['attrInfo']['suk'] . ')';
                    }
                }
                $goodsName[] = implode(
                    [$v['productInfo']['store_name'],
                        $suk,
                        "[{$v['cart_num']} * {$v['truePrice']}]"
                    ], ' ');
            }
            $item['cartInfo'] = $_info;
            $sex = WechatUser::where('uid', $item['uid'])->value('sex');
            if ($sex == 1) $sex_name = '男';
            else if ($sex == 2) $sex_name = '女';
            else $sex_name = '未知';
            $export[] = [
                $item['order_id'],
                $sex_name,
                $item['phone'],
                $item['real_name'],
                $item['user_phone'],
                $item['user_address'],
                $goodsName,
                $item['total_price'],
                $item['pay_price'],
                $item['pay_postage'],
                $item['pay_type_name'],
                $item['pay_time'] > 0 ? date('Y/m-d H:i', $item['pay_time']) : '暂无',
                $item['status_name'],
                $item['add_time'],
                $item['mark']
            ];
        }
        PHPExcelService::setExcelHeader(['订单号', '性别', '电话', '收货人姓名', '收货人电话', '收货地址', '商品信息',
            '总价格', '实际支付', '邮费', '支付状态', '支付时间', '订单状态', '下单时间', '用户备注'])
            ->setExcelTile('订单导出' . date('YmdHis', time()), '订单信息' . time(), ' 生成时间：' . date('Y-m-d H:i:s', time()))
            ->setExcelContent($export)
            ->ExcelSave();
    }

    /**
     * @param $where
     * @return array
     */
    public static function systemPage($where, $userid = false)
    {
        $model = self::getOrderWhere($where, self::alias('a')
            ->join('user r', 'r.uid=a.uid', 'LEFT'), 'a.', 'r')
            ->field('a.*,r.nickname');
        if ($where['order']) {
            $model = $model->order('a.' . $where['order']);
        } else {
            $model = $model->order('a.id desc');
        }
        if ($where['export'] == 1) {
            $list = $model->select()->toArray();
            $export = [];
            foreach ($list as $index => $item) {

                if ($item['pay_type'] == 'weixin') {
                    $payType = '微信支付';
                } elseif ($item['pay_type'] == 'yue') {
                    $payType = '余额支付';
                } elseif ($item['pay_type'] == 'offline') {
                    $payType = '线下支付';
                } else {
                    $payType = '其他支付';
                }

                $_info = StoreOrderCartInfo::where('oid', $item['id'])->column('cart_info', 'oid');
                $goodsName = [];
                foreach ($_info as $k => $v) {
                    $v = json_decode($v, true);
                    $goodsName[] = implode(
                        [$v['productInfo']['store_name'],
                            isset($v['productInfo']['attrInfo']) ? '(' . $v['productInfo']['attrInfo']['suk'] . ')' : '',
                            "[{$v['cart_num']} * {$v['truePrice']}]"
                        ], ' ');
                }
                $item['cartInfo'] = $_info;
                $export[] = [
                    $item['order_id'], $payType,
                    $item['total_num'], $item['total_price'], $item['total_postage'], $item['pay_price'], $item['refund_price'],
                    $item['mark'], $item['remark'],
                    [$item['real_name'], $item['user_phone'], $item['user_address']],
                    $goodsName,
                    [$item['paid'] == 1 ? '已支付' : '未支付', '支付时间: ' . ($item['pay_time'] > 0 ? date('Y/md H:i', $item['pay_time']) : '暂无')]

                ];
                $list[$index] = $item;
            }
            PHPExcelService::setExcelHeader(['订单号', '支付方式', '商品总数', '商品总价', '邮费', '支付金额', '退款金额', '用户备注', '管理员备注', '收货人信息', '商品信息', '支付状态'])
                ->setExcelTile('订单导出', '订单信息' . time(), ' 生成时间：' . date('Y-m-d H:i:s', time()))
                ->setExcelContent($export)
                ->ExcelSave();
        }
        return self::page($model, function ($item) {
            $_info = StoreOrderCartInfo::where('oid', $item['id'])->field('cart_info')->select();
            foreach ($_info as $k => $v) {
                $_info[$k]['cart_info'] = json_decode($v['cart_info'], true);
            }
            $item['_info'] = $_info;


                    $item['pink_name'] = '[普通订单]';
                    $item['color'] = '#895612';


        }, $where);
    }

    public static function statusByWhere($status, $model = null, $alert = '')
    {
        if ($model == null) $model = new self;
        if ('' === $status)
            return $model;
        else if ($status == 8)
            return $model;
        else if ($status == 0)//未支付
            return $model->where($alert . 'paid', 0)->where($alert . 'status', 0)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == 1)//已支付 未发货
            return $model->where($alert . 'paid', 1)->where($alert . 'status', 0)->where($alert . 'shipping_type', 1)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == 2)//已支付  待收货
            return $model->where($alert . 'paid', 1)->where($alert . 'status', 1)->where($alert . 'shipping_type', 1)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == 5)//已支付  待核销
            return $model->where($alert . 'paid', 1)->where($alert . 'status', 0)->where($alert . 'shipping_type', 2)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == 3)// 已支付  已收货  待评价
            return $model->where($alert . 'paid', 1)->where($alert . 'status', 2)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == 4)// 交易完成
            return $model->where($alert . 'paid', 1)->where($alert . 'status', 3)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else if ($status == -1)//退款中
            return $model->where($alert . 'paid', 1)->where($alert . 'refund_status', 1)->where($alert . 'is_del', 0);
        else if ($status == -2)//已退款
            return $model->where($alert . 'paid', 1)->where($alert . 'refund_status', 2)->where($alert . 'is_del', 0);
        else if ($status == -3)//退款
            return $model->where($alert . 'paid', 1)->where($alert . 'refund_status', 'in', '1,2')->where($alert . 'is_del', 0);
        else if ($status == -4)//已删除
            return $model->where($alert . 'is_del', 1);
        else if ($status == 9)//已卖出
            return $model->where($alert . 'paid', 1)->where($alert . 'refund_status', 0)->where($alert . 'is_del', 0);
        else
            return $model;
    }

    public static function changeOrderId($orderId)
    {
        $ymd = substr($orderId, 2, 8);
        $key = substr($orderId, 16);
        return 'wx' . $ymd . date('His') . $key;
    }

    /**
     * 线下付款
     * @param $id
     * @return $this
     */
    public static function updateOffline($id)
    {
        $count = self::where('id', $id)->count();
        if (!$count) return self::setErrorInfo('订单不存在');
        $count = self::where('id', $id)->where('paid', 0)->count();
        if (!$count) return self::setErrorInfo('订单已支付');
        $res = self::where('id', $id)->update(['paid' => 1, 'pay_time' => time()]);
        return $res;
    }

    /**
     * 处理where条件
     * @param $where
     * @param $model
     * @return mixed
     */
    public static function getOrderWhere($where, $model, $aler = '', $join = '')
    {
//        $model = $model->where('combination_id',0);
        $model = $model->where('is_system_del', 0);
        if (isset($where['status']) && $where['status'] != '') {
            $model = self::statusByWhere($where['status'], $model, $aler);
        }
        if (isset($where['is_del']) && $where['is_del'] != '' && $where['is_del'] != -1) $model = $model->where($aler . 'is_del', $where['is_del']);
        if (isset($where['pay_type'])) {
            switch ($where['pay_type']) {
                case 1:
                    $model = $model->where($aler . 'pay_type', 'weixin');
                    break;
                case 2:
                    $model = $model->where($aler . 'pay_type', 'yue');
                    break;
                case 3:
                    $model = $model->where($aler . 'pay_type', 'offline');
                    break;
            }
        }
        if (isset($where['type'])) {
            switch ($where['type']) {
                case 1:

                    break;
                case 2:
//
                    break;
                case 3:

                    break;
                case 4:

                    break;
            }
        }

        if (isset($where['real_name']) && $where['real_name'] != '') {
            $model = $model->where($aler . 'order_id|' . $aler . 'real_name|' . $aler . 'user_phone' . ($join ? '|' . $join . '.nickname|' . $join . '.uid|' . $join . '.phone' : ''), 'LIKE', "%$where[real_name]%");
        }
        if (isset($where['data']) && $where['data'] !== '') {
            $model = self::getModelTime($where, $model, $aler . 'add_time');
        }

        if (isset($where['mer_id']) && $where['mer_id'] != '') {
            if($where['mer_id']==-1){
                $model = $model->where('mer_id' ,'<>', 0);
            }else{
                $model = $model->where(['mer_id' => $where['mer_id']]);
            }
        }else{
            $model = $model->where('mer_id' ,'<>', 0);
        }
        return $model;
    }

    public static function getBadge($where)
    {
        $price = self::getOrderPrice($where);
        return [
            [
                'name' => '订单数量',
                'field' => '件',
                'count' => $price['count_sum'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '售出商品',
                'field' => '件',
                'count' => $price['total_num'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '订单金额',
                'field' => '元',
                'count' => $price['pay_price'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '退款金额',
                'field' => '元',
                'count' => $price['refund_price'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '微信支付金额',
                'field' => '元',
                'count' => $price['pay_price_wx'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '余额支付金额',
                'field' => '元',
                'count' => $price['pay_price_yue'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '运费金额',
                'field' => '元',
                'count' => $price['pay_postage'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '线下支付金额',
                'field' => '元',
                'count' => $price['pay_price_offline'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '积分抵扣',
                'field' => '分',
                'count' => $price['use_integral'] . '(抵扣金额:￥' . $price['deduction_price'] . ')',
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ],
            [
                'name' => '退回积分',
                'field' => '元',
                'count' => $price['back_integral'],
                'background_color' => 'layui-bg-blue',
                'col' => 2
            ]
        ];
    }

    /**
     * 处理订单金额
     * @param $where
     * @return array
     */
    public static function getOrderPrice($where)
    {
        $where['is_del'] = 0;//删除订单不统计
        $model = new self;
        $price = [];
        $price['pay_price'] = 0;//支付金额
        $price['refund_price'] = 0;//退款金额
        $price['pay_price_wx'] = 0;//微信支付金额
        $price['pay_price_yue'] = 0;//余额支付金额
        $price['pay_price_offline'] = 0;//线下支付金额
        $price['pay_price_other'] = 0;//其他支付金额
        $price['use_integral'] = 0;//用户使用积分
        $price['back_integral'] = 0;//退积分总数
        $price['deduction_price'] = 0;//抵扣金额
        $price['total_num'] = 0; //商品总数
        $price['count_sum'] = 0; //商品总数
        $price['brokerage'] = 0;
        $price['pay_postage'] = 0;
        $whereData = ['is_del' => 0];
        if ($where['status'] == '') {
            $whereData['paid'] = 1;
            $whereData['refund_status'] = 0;
        }
        $ids = self::getOrderWhere($where, $model)->where($whereData)->column('id');
        if (count($ids)) {
            $price['brokerage'] = UserBill::where(['category' => 'now_money', 'type' => 'brokerage'])->where('link_id', 'in', $ids)->sum('number');
        }
        $price['refund_price'] = self::getOrderWhere($where, $model)->where(['is_del' => 0, 'paid' => 1, 'refund_status' => 2])->sum('refund_price');
        $sumNumber = self::getOrderWhere($where, $model)->where($whereData)->field([
            'sum(total_num) as sum_total_num',
            'count(id) as count_sum',
            'sum(pay_price) as sum_pay_price',
            'sum(pay_postage) as sum_pay_postage',
            'sum(use_integral) as sum_use_integral',
            'sum(back_integral) as sum_back_integral',
            'sum(deduction_price) as sum_deduction_price'
        ])->find();
        if ($sumNumber) {
            $price['count_sum'] = $sumNumber['count_sum'];
            $price['total_num'] = $sumNumber['sum_total_num'];
            $price['pay_price'] = $sumNumber['sum_pay_price'];
            $price['pay_postage'] = $sumNumber['sum_pay_postage'];
            $price['use_integral'] = $sumNumber['sum_use_integral'];
            $price['back_integral'] = $sumNumber['sum_back_integral'];
            $price['deduction_price'] = $sumNumber['sum_deduction_price'];
        }
        $list = self::getOrderWhere($where, $model)->where($whereData)->group('pay_type')->column('sum(pay_price) as sum_pay_price,pay_type', 'id');
        foreach ($list as $v) {
            if ($v['pay_type'] == 'weixin') {
                $price['pay_price_wx'] = $v['sum_pay_price'];
            } elseif ($v['pay_type'] == 'yue') {
                $price['pay_price_yue'] = $v['sum_pay_price'];
            } elseif ($v['pay_type'] == 'offline') {
                $price['pay_price_offline'] = $v['sum_pay_price'];
            } else {
                $price['pay_price_other'] = $v['sum_pay_price'];
            }
        }
        return $price;
    }

    /**
     * 处理where条件
     * @param $where
     * @param $model
     * @return mixed
     */
    public static function getOrderWherePink($where, $model)
    {

        if ($where['status'] != '') $model = $model::statusByWhere($where['status']);
//        if($where['is_del'] != '' && $where['is_del'] != -1) $model = $model->where('is_del',$where['is_del']);
        if ($where['real_name'] != '') {
            $model = $model->where('order_id|real_name|user_phone', 'LIKE', "%$where[real_name]%");
        }
        if ($where['data'] !== '') {
            $model = self::getModelTime($where, $model, 'add_time');
        }
        return $model;
    }


    /**
     * 设置订单统计图搜索
     * @param array $where 条件
     * @param null $status
     * @param null $time
     * @return array
     */
    public static function setEchatWhere($where, $status = null, $time = null)
    {
        $model = self::statusByWhere($where['status'])->where('is_system_del', 0);
        if ($status !== null) $where['type'] = $status;
        if ($time === true) $where['data'] = '';
        switch ($where['type']) {
            case 1:
                //普通商品

                break;
            case 2:

                break;
            case 3:

                break;
            case 4:

                break;
        }
        return self::getModelTime($where, $model);
    }

    /*
     * 获取订单数据统计图
     * $where array
     * $limit int
     * return array
     */
    public static function getEchartsOrder($where, $limit = 20)
    {
        $orderlist = self::setEchatWhere($where)->field(
            'FROM_UNIXTIME(add_time,"%Y-%m-%d") as _add_time,sum(total_num) total_num,count(*) count,sum(total_price) total_price,sum(refund_price) refund_price,group_concat(cart_id SEPARATOR "|") cart_ids'
        )->group('_add_time')->order('_add_time asc')->select();
        count($orderlist) && $orderlist = $orderlist->toArray();
        $legend = ['商品数量', '订单数量', '订单金额', '退款金额'];
        $seriesdata = [
            [
                'name' => $legend[0],
                'type' => 'line',
                'data' => [],
            ],
            [
                'name' => $legend[1],
                'type' => 'line',
                'data' => []
            ],
            [
                'name' => $legend[2],
                'type' => 'line',
                'data' => []
            ],
            [
                'name' => $legend[3],
                'type' => 'line',
                'data' => []
            ]
        ];
        $xdata = [];
        $zoom = '';
        foreach ($orderlist as $item) {
            $xdata[] = $item['_add_time'];
            $seriesdata[0]['data'][] = $item['total_num'];
            $seriesdata[1]['data'][] = $item['count'];
            $seriesdata[2]['data'][] = $item['total_price'];
            $seriesdata[3]['data'][] = $item['refund_price'];
        }
        count($xdata) > $limit && $zoom = $xdata[$limit - 5];
        $badge = self::getOrderBadge($where);
        $bingpaytype = self::setEchatWhere($where)->group('pay_type')->field('count(*) as count,pay_type')->select();
        count($bingpaytype) && $bingpaytype = $bingpaytype->toArray();
        $bing_xdata = ['微信支付', '余额支付', '其他支付'];
        $color = ['#ffcccc', '#99cc00', '#fd99cc', '#669966'];
        $bing_data = [];
        foreach ($bingpaytype as $key => $item) {
            if ($item['pay_type'] == 'weixin') {
                $value['name'] = $bing_xdata[0];
            } else if ($item['pay_type'] == 'yue') {
                $value['name'] = $bing_xdata[1];
            } else {
                $value['name'] = $bing_xdata[2];
            }
            $value['value'] = $item['count'];
            $value['itemStyle']['color'] = isset($color[$key]) ? $color[$key] : $color[0];
            $bing_data[] = $value;
        }
        return compact('zoom', 'xdata', 'seriesdata', 'badge', 'legend', 'bing_data', 'bing_xdata');
    }

    public static function getOrderBadge($where)
    {
        return [
            [
                'name' => '普通订单数量',
                'field' => '个',
                'count' => self::setEchatWhere($where, 1)->count(),
                'content' => '普通总订单数量',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, 1, true)->count(),
                'class' => 'fa fa-line-chart',
                'col' => 2,
            ],
            [
                'name' => '积分消耗数',
                'field' => '个',
                'count' => self::setEchatWhere($where)->sum('use_integral'),
                'content' => '积分消耗总数',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->sum('use_integral'),
                'class' => 'fa fa-line-chart',
                'col' => 2
            ],
            [
                'name' => '积分抵扣金额',
                'field' => '个',
                'count' => self::setEchatWhere($where)->sum('deduction_price'),
                'content' => '积分抵扣总金额',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->sum('deduction_price'),
                'class' => 'fa fa-money',
                'col' => 2
            ],
            [
                'name' => '在线支付金额',
                'field' => '元',
                'count' => self::setEchatWhere($where)->where(['paid' => 1, 'refund_status' => 0])->where('pay_type', 'weixin')->sum('pay_price'),
                'content' => '在线支付总金额',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->where(['paid' => 1, 'refund_status' => 0])->where('pay_type', 'weixin')->sum('pay_price'),
                'class' => 'fa fa-weixin',
                'col' => 2
            ],
            [
                'name' => '余额支付金额',
                'field' => '元',
                'count' => self::setEchatWhere($where)->where('pay_type', 'yue')->where(['paid' => 1, 'refund_status' => 0])->sum('pay_price'),
                'content' => '余额支付总金额',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->where(['paid' => 1, 'refund_status' => 0])->where('pay_type', 'yue')->sum('pay_price'),
                'class' => 'fa  fa-balance-scale',
                'col' => 2
            ],
            [
                'name' => '赚取积分',
                'field' => '分',
                'count' => self::setEchatWhere($where)->sum('gain_integral'),
                'content' => '赚取总积分',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->sum('gain_integral'),
                'class' => 'fa fa-gg-circle',
                'col' => 2
            ],
            [
                'name' => '交易额',
                'field' => '元',
                'count' => self::setEchatWhere($where)->where(['paid' => 1, 'refund_status' => 0])->sum('pay_price'),
                'content' => '总交易额',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->where(['paid' => 1, 'refund_status' => 0])->sum('pay_price'),
                'class' => 'fa fa-jpy',
                'col' => 2
            ],
            [
                'name' => '订单商品数量',
                'field' => '元',
                'count' => self::setEchatWhere($where)->sum('total_num'),
                'content' => '订单商品总数量',
                'background_color' => 'layui-bg-cyan',
                'sum' => self::setEchatWhere($where, null, true)->sum('total_num'),
                'class' => 'fa fa-cube',
                'col' => 2
            ]
        ];
    }



    /**
     * 获取订单总数
     * @param int $uid
     * @return int|string
     */
    public static function getOrderCount($uid = 0)
    {
        if (!$uid) return 0;
        return self::where('uid', $uid)->where('paid', 1)->where('refund_status', 0)->where('status', 2)->count();
    }

}