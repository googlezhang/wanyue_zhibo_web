<?php

namespace app\admin\model\live;

use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;
use app\models\user\User;
use app\models\live\LiveClass;

/**
 * Class LiveRecord
 * @package app\admin\model\store
 */
class LiveRecord extends BaseModel
{

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'live_record';

    use ModelTrait;


    /**
     * 获取连表MOdel
     * @param $model
     * @return object
     */
    public static function getModelObject($where = [])
    {
        $model = new self();
        if (!empty($where)) {
            if (isset($where['uid']) && $where['uid'] != '') {
                $model = $model->where('uid', $where['uid']);
            }
        }
        return $model;
    }

    /**
     * 异步获取列表
     * @param $where
     * @return array
     */
    public static function getList($where)
    {
        $classlist=LiveClass::getList();

        $model = self::getModelObject($where);
        if(isset($where['page']) && $where['page']) $model = $model->page((int)$where['page'], (int)$where['limit']);
        $model = $model->order('id desc');

        $data = ($data =$model->select()) && count($data) ? $data->toArray() : [];
        foreach ($data as &$item) {
            $nickname='';
            $avatar='';
            $userinfo=User::getUserInfo($item['uid'],'uid,nickname,avatar');
            if($userinfo){
                $nickname=$userinfo['nickname'];
                $avatar=$userinfo['avatar'];
            }
            $item['nickname']=$nickname;
            $item['avatar']=$avatar;
            $item['start_time']=date('Y-m-d H:i:s',$item['starttime']);
            $item['end_time']=date('Y-m-d H:i:s',$item['endtime']);
            $classname='未分类';
            foreach ($classlist as $v){
                if($v['id']==$item['classid']){
                    $classname=$v['name'];
                    break;
                }
            }
            $item['classname']=$classname;

        }

        $count = self::getModelObject($where)->count();
        return compact('count', 'data');
    }


    public static function delid($id)
    {
        return self::del($id);
    }

}