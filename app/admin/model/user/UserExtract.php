<?php
/**
 * Created by PhpStorm.
 * User: lianghuan
 * Date: 2018-03-03
 * Time: 16:47
 */

namespace app\admin\model\user;

use app\admin\model\wechat\WechatUser;
use think\facade\Route as Url;
use wanyue\traits\ModelTrait;
use wanyue\basic\BaseModel;

/**
 * 用户提现管理 model
 * Class User
 * @package app\admin\model\user
 */
class UserExtract extends BaseModel
{
    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'user_extract';

    use ModelTrait;

    /**
     * 获得用户提现总金额
     * @param $uid
     * @return mixed
     */
    public static function userExtractTotalPrice($uid, $status = 1, $where = [])
    {
        return self::getModelTime($where, self::where('uid', 'in', $uid)->where('status', $status))->sum('extract_price') ?: 0;
    }

    public static function extractStatistics()
    {
        //待提现金额
        $data['price'] = floatval(self::where('status', 0)->sum('extract_price'));
        //佣金总金额
        $data['brokerage_count'] = floatval(UserBill::getBrokerageCount());
        //已提现金额
        $data['priced'] = floatval(self::where('status', 1)->sum('extract_price'));
        //未提现金额
        $data['brokerage_not'] = bcsub(bcsub($data['brokerage_count'], $data['priced'], 2), $data['price'], 2);
        return compact('data');
    }

    /**
     * @param $where
     * @return array
     */
    public static function systemPage($where)
    {
        $model = new self;
        if ($where['date'] != '') {
            list($startTime, $endTime) = explode(' - ', $where['date']);
            $model = $model->where('a.add_time', '>', strtotime($startTime));
            $model = $model->where('a.add_time', '<', (int)bcadd(strtotime($endTime), 86400, 0));
        }
        if ($where['status'] != '') $model = $model->where('a.status', $where['status']);
        if ($where['extract_type'] != '') $model = $model->where('a.extract_type', $where['extract_type']);
        if ($where['nireid'] != '') $model = $model->where('a.real_name|a.id|b.nickname|a.bank_code|a.alipay_code', 'like', "%$where[nireid]%");
        $model = $model->alias('a');
        $model = $model->field('a.*,b.nickname');
        $model = $model->join('user b', 'b.uid=a.uid', 'LEFT');
        $model = $model->order('a.id desc');
        return self::page($model, $where);
    }

    public static function changeFail($id, $fail_msg)
    {
        $fail_time = time();
        $data = self::get($id);
        $extract_number = $data['extract_price'];
        $mark = '提现失败,退回佣金' . $extract_number . '元';
        $uid = $data['uid'];
        $status = -1;
        $User = User::where('uid', $uid)->find()->toArray();
        UserBill::income('提现失败', $uid, 'now_money', 'extract', $extract_number, $id, bcadd($User['now_money'], $extract_number, 2), $mark);
        User::bcInc($uid, 'brokerage_price', $extract_number, 'uid');
        $extract_type = '未知方式';
        switch ($data['extract_type']) {
            case 'alipay':
                $extract_type = '支付宝';
                break;
            case 'bank':
                $extract_type = '银行卡';
                break;
            case 'weixin':
                $extract_type = '微信';
                break;
        }

        return self::edit(compact('fail_time', 'fail_msg', 'status'), $id);
    }

    public static function changeSuccess($id)
    {

        $data = self::get($id);
        $extractNumber = $data['extract_price'];
        $mark = '成功提现佣金' . $extractNumber . '元';
        $wechatUserInfo = WechatUser::where('uid', $data['uid'])->field('openid,user_type,routine_openid,nickname')->find();
        $extract_type = '未知方式';
        switch ($data['extract_type']) {
            case 'alipay':
                $extract_type = '支付宝';
                break;
            case 'bank':
                $extract_type = '银行卡';
                break;
            case 'weixin':
                $extract_type = '微信';
                break;
        }

        return self::edit(['status' => 1], $id);
    }

    //测试数据
    public static function test()
    {
        $uids = User::order('uid desc')->limit(2, 20)->field(['uid', 'nickname'])->select()->toArray();
        $type = ['bank', 'alipay', 'weixin'];
        foreach ($uids as $item) {
            $data = [
                'uid' => $item['uid'],
                'real_name' => $item['nickname'],
                'extract_type' => isset($type[rand(0, 2)]) ? $type[rand(0, 2)] : 'alipay',
                'bank_code' => rand(1000000, 999999999),
                'bank_address' => '中国',
                'alipay_code' => rand(1000, 9999999),
                'extract_price' => rand(100, 9999),
                'mark' => '测试数据',
                'add_time' => time(),
                'status' => 1,
                'wechat' => rand(999, 878788) . $item['uid'],
            ];
            self::create($data);
        }
    }

    /**
     * 获取用户累计提现金额
     * @param int $uid
     * @return int|mixed
     */
    public static function getUserCountPrice($uid = 0)
    {
        if (!$uid) return 0;
        $price = self::where('uid', $uid)->where('status', 1)->sum('extract_price');
        return $price ? $price : 0;
    }

    /**
     * 获取用户累计提现次数
     * @param int $uid
     * @return int|string
     */
    public static function getUserCountNum($uid = 0)
    {
        if (!$uid) return 0;
        return self::where('uid', $uid)->count();
    }
}