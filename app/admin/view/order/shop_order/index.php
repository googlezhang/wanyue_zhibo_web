{extend name="public/container"}
{block name="head_top"}

{/block}
{block name="content"}
<style>
    .btn-outline{
        border:none;
    }
    .btn-outline:hover{
        background-color: #0e9aef;
        color: #fff;
    }
    .layui-form-item .layui-btn {
        margin-top: 5px;
        margin-right: 10px;
    }
    .layui-btn-primary{
        margin-right: 10px;
        margin-left: 0!important;
    }
    label{
        margin-bottom: 0!important;
        margin-top: 4px;
    }
</style>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15" id="app">
        <!--搜索条件-->
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">搜索条件</div>
                <div class="layui-card-body">
                    <div class="layui-carousel layadmin-carousel layadmin-shortcut" lay-anim="" lay-indicator="inside"
                         lay-arrow="none" style="background:none">
                        <div class="layui-card-body">
                            <div class="layui-row layui-col-space10 layui-form-item">
                                <div class="layui-col-lg12">
                                    <label class="layui-form-label">订单状态:</label>
                                    <div class="layui-input-block" v-cloak="">
                                        <button class="layui-btn layui-btn-sm"
                                                :class="{'layui-btn-primary':where.status!==item.value}"
                                                @click="where.status = item.value" type="button"
                                                v-for="item in orderStatus">{{item.name}}
                                        </button>
                                    </div>
                                </div>
                                <div class="layui-col-lg12">
                                    <label class="layui-form-label">订单号:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="real_name" style="width: 50%" v-model="where.real_name"
                                               placeholder="请输入姓名、电话、订单编号" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-lg12">
                                    <label class="layui-form-label">店铺ID:</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="mer_id" style="width: 50%" v-model="where.mer_id"
                                               placeholder="请输入店铺ID" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-col-lg12">
                                    <div class="layui-input-block">
                                        <button @click="search" type="button"
                                                class="layui-btn layui-btn-sm layui-btn-normal">
                                            <i class="layui-icon layui-icon-search"></i>搜索
                                        </button>
                                        <button @click="excel" type="button"
                                                class="layui-btn layui-btn-warm layui-btn-sm export" type="button">
                                            <i class="fa fa-floppy-o" style="margin-right: 3px;"></i>导出
                                        </button>
                                        <button @click="refresh" type="reset"
                                                class="layui-btn layui-btn-primary layui-btn-sm">
                                            <i class="layui-icon layui-icon-refresh"></i>刷新
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end-->
        <!-- 中间详细信息-->
        <div :class="item.col!=undefined ? 'layui-col-sm'+item.col+' '+'layui-col-md'+item.col:'layui-col-sm6 layui-col-md3'"
             v-for="item in badge" v-cloak="" v-if="item.count > 0">
            <div class="layui-card">
                <div class="layui-card-header">
                    {{item.name}}
                    <span class="layui-badge layuiadmin-badge" :class="item.background_color">{{item.field}}</span>
                </div>
                <div class="layui-card-body">
                    <p class="layuiadmin-big-font">{{item.count}}</p>
                    <p v-show="item.content!=undefined">
                        {{item.content}}
                        <span class="layuiadmin-span-color">{{item.sum}}<i :class="item.class"></i></span>
                    </p>
                </div>
            </div>
        </div>
        <!--enb-->
    </div>
    <!--列表-->
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">订单列表</div>
                <div class="layui-card-body">

                    <table class="layui-hide" id="List" lay-filter="List"></table>
                    <!--订单-->
                    <script type="text/html" id="order_id">
                        {{d.order_id}}<br/>
                        <span style="color: {{d.color}};">{{d.pink_name}}</span><br/>　
                        {{#  if(d.is_del == 1){ }}<span style="color: {{d.color}};">用户已删除</span>{{# } }}　
                    </script>
                    <!--用户信息-->
                    <script type="text/html" id="userinfo">
                        {{d.nickname==null ? '暂无信息':d.nickname}}/{{d.uid}}
                    </script>

                    <!--支付状态-->
                    <script type="text/html" id="paid">
                        {{#  if(d.pay_type==1){ }}
                        <p>{{d.pay_type_name}}</p>
                        {{#  }else{ }}
                        {{# if(d.pay_type_info!=undefined){ }}
                        <p><span>线下支付</span></p>
                        {{# }else{ }}
                        <p>{{d.pay_type_name}}</p>
                        {{# } }}
                        {{# }; }}
                    </script>
                    <!--订单状态-->
                    <script type="text/html" id="status">
                        {{d.status_name}}
                    </script>
                    <!--商品信息-->
                    <script type="text/html" id="info">
                        {{#  layui.each(d._info, function(index, item){ }}
                        {{#  if(item.cart_info.productInfo.attrInfo!=undefined){ }}
                        <div>
                            <span>
                                <img style="width: 30px;height: 30px;margin:0;cursor: pointer;"
                                     src="{{item.cart_info.productInfo.attrInfo.image}}">
                            </span>
                            <span>{{item.cart_info.productInfo.store_name}}&nbsp;{{item.cart_info.productInfo.attrInfo.suk}}</span>
                            <span> | ￥{{item.cart_info.truePrice}}×{{item.cart_info.cart_num}}</span>
                        </div>
                        {{#  }else{ }}
                        <div>
                            <span><img style="width: 30px;height: 30px;margin:0;cursor: pointer;"
                                       src="{{item.cart_info.productInfo.image}}"></span>
                            <span>{{item.cart_info.productInfo.store_name}}</span><span> | ￥{{item.cart_info.truePrice}}×{{item.cart_info.cart_num}}</span>
                        </div>
                        {{# } }}
                        {{#  }); }}
                    </script>

                    <script type="text/html" id="act">
                        <button type="button" class="layui-btn layui-btn-xs" onclick="dropdown(this)">操作 <span
                                    class="caret"></span></button>
                        <ul class="layui-nav-child layui-anim layui-anim-upbit">
                            <li>
                                <a href="javascript:void(0);" lay-event='order_info'>
                                    <i class="fa fa-file-text"></i> 订单详情
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);"
                                   onclick="$eb.createModalFrame('订单记录','{:Url('order_status')}?oid={{d.id}}')">
                                    <i class="fa fa-newspaper-o"></i> 订单记录
                                </a>
                            </li>
                        </ul>
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!--end-->
</div>
<script src="{__ADMIN_PATH}js/layuiList.js"></script>
{/block}
{block name="script"}
<script>
    layList.tableList('List', "{:Url('order_list',['real_name'=>$real_name])}", function () {
        return [
            {type: 'checkbox'},
            {field: 'order_id', title: '订单号', sort: true, event: 'order_id', width: '14%', templet: '#order_id'},
            {field: 'shopname', title: '店铺信息', width: '10%', align: 'center'},
            {field: 'nickname', title: '用户信息', templet: '#userinfo', width: '10%', align: 'center'},
            {field: 'info', title: '商品信息', templet: "#info", height: 'full-20'},
            {field: 'pay_price', title: '实际支付', width: '8%', align: 'center'},
            {field: 'paid', title: '支付状态', templet: '#paid', width: '8%', align: 'center'},
            {field: 'status', title: '订单状态', templet: '#status', width: '8%', align: 'center'},
            {field: 'add_time', title: '下单时间', width: '10%', sort: true, align: 'center'},
            {field: 'right', title: '操作', align: 'center', toolbar: '#act', width: '10%'},
        ];
    });
    layList.tool(function (event, data, obj) {
        switch (event) {
            case 'order_info':
                $eb.createModalFrame(data.nickname + '订单详情', layList.U({a: 'order_info', q: {oid: data.id}}));
                break;
        }
    })
    //下拉框
    $(document).click(function (e) {
        $('.layui-nav-child').hide();
    })

    function dropdown(that) {
        var oEvent = arguments.callee.caller.arguments[0] || event;
        oEvent.stopPropagation();
        var offset = $(that).offset();
        var top = offset.top - $(window).scrollTop();
        var index = $(that).parents('tr').data('index');
        $('.layui-nav-child').each(function (key) {
            if (key != index) {
                $(this).hide();
            }
        })
        if ($(document).height() < top + $(that).next('ul').height()) {
            $(that).next('ul').css({
                'padding': 10,
                'top': -($(that).parents('td').height() / 2 + $(that).height() + $(that).next('ul').height() / 2),
                'min-width': 'inherit',
                'position': 'absolute'
            }).toggle();
        } else {
            $(that).next('ul').css({
                'padding': 10,
                'top': $(that).parents('td').height() / 2 + $(that).height(),
                'min-width': 'inherit',
                'position': 'absolute'
            }).toggle();
        }
    }

    var real_name = '<?=$real_name?>';
    var status =<?=$status ? $status : "''"?>;
    require(['vue'], function (Vue) {
        new Vue({
            el: "#app",
            data: {
                badge: [],
                orderStatus: [
                    {name: '全部', value: ''},
                    {name: '已卖出', value: 9},
                    {name: '未发货', value: 1},
                    /*{name: '待核销', value: 5},*/
                    {name: '未支付', value: 0},
                    {name: '待收货', value: 2},
                    {name: '待评价', value: 3},
                    {name: '交易完成', value: 4},
                    {name: '退款中', value: -1},
                    {name: '已退款', value: -2},
                    {name: '已删除', value: -4},
                ],
                where: {
                    data: '',
                    status: status,
                    type: '',
                    pay_type: '',
                    real_name: real_name || '',
                    excel: 0,
                },
                showtime: false,
            },
            watch: {
                'where.status': function () {
                    this.where.excel = 0;
                    this.getBadge();
                    layList.reload(this.where, true);
                },
            },
            methods: {
                setData: function (item) {
                    var that = this;
                    if (item.is_zd == true) {
                        that.showtime = true;
                        this.where.data = this.$refs.date_time.innerText;
                    } else {
                        this.showtime = false;
                        this.where.data = item.value;
                    }
                },
                getBadge: function () {
                    var that = this;
                    layList.basePost(layList.Url({c: 'order.shop_order', a: 'getBadge'}), this.where, function (rem) {
                        that.badge = rem.data;
                    });
                },
                search: function () {
                    this.where.excel = 0;
                    this.getBadge();
                    layList.reload(this.where, true);
                },
                refresh: function () {
                    layList.reload();
                    this.getBadge();
                },
                excel: function () {
                    this.where.excel = 1;
                    location.href = layList.U({c: 'order.shop_order', a: 'order_list', q: this.where});
                    this.where.excel = 0;
                }
            },
            mounted: function () {
                var that = this;
                that.getBadge();
                window.formReload = this.search;
                layList.laydate.render({
                    elem: this.$refs.date_time,
                    trigger: 'click',
                    eventElem: this.$refs.time,
                    range: true,
                    change: function (value) {
                        that.where.data = value;
                    }
                });
            }
        })
    });
</script>
{/block}