<?php

use think\facade\Route;
//账号密码登录
Route::post('login', 'AuthController/login')->name('login')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);

// 获取发短信的key
Route::get('verify_code', 'AuthController/verifyCode')->name('verifyCode')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);

//手机号登录
Route::post('login/mobile', 'AuthController/mobile')->name('loginMobile')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);

//图片验证码
Route::get('sms_captcha', 'AuthController/captcha')->name('captcha')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);
//验证码发送
Route::post('register/verify', 'AuthController/verify')->name('registerVerify')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);
//手机号注册
Route::post('register', 'AuthController/register')->name('register')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);

//手机号修改密码
Route::post('register/reset', 'AuthController/reset')->name('registerReset')
    ->middleware(\app\http\middleware\AllowOriginMiddleware::class);

Route::any('wxapp/notify', 'wechat.AuthController/notify_app');//微信APP支付回调
Route::any('alipay/notify', 'alipay.AuthController/notify_app');//支付宝APP支付回调
Route::any('liveback/tx', 'live.LiveBackController/back_tx');//腾讯直播回调
Route::any('liveback/uplive', 'live.LiveBackController/uplive');//socket计时关播

//管理员订单操作类 
Route::group(function () {
    Route::get('admin/order/statistics', 'admin.StoreOrderController/statistics')->name('adminOrderStatistics');//订单数据统计
    Route::get('admin/order/data', 'admin.StoreOrderController/data')->name('adminOrderData');//订单每月统计数据
    Route::get('admin/order/list', 'admin.StoreOrderController/lst')->name('adminOrderList');//订单列表
    Route::get('admin/order/detail/:orderId', 'admin.StoreOrderController/detail')->name('adminOrderDetail');//订单详情
    Route::get('admin/order/delivery/gain/:orderId', 'admin.StoreOrderController/delivery_gain')->name('adminOrderDeliveryGain');//订单发货获取订单信息
    Route::post('admin/order/delivery/keep', 'admin.StoreOrderController/delivery_keep')->name('adminOrderDeliveryKeep');//订单发货
    Route::post('admin/order/price', 'admin.StoreOrderController/price')->name('adminOrderPrice');//订单改价
    Route::post('admin/order/remark', 'admin.StoreOrderController/remark')->name('adminOrderRemark');//订单备注
    Route::get('admin/order/time', 'admin.StoreOrderController/time')->name('adminOrderTime');//订单交易额时间统计
    Route::post('admin/order/offline', 'admin.StoreOrderController/offline')->name('adminOrderOffline');//订单支付
    Route::post('admin/order/refund', 'admin.StoreOrderController/refund')->name('adminOrderRefund');//订单退款
    Route::post('order/order_verific','admin.StoreOrderController/order_verific')->name('order');//订单核销
})->middleware(\app\http\middleware\AllowOriginMiddleware::class)->middleware(\app\http\middleware\AuthTokenMiddleware::class, true)->middleware(\app\http\middleware\CustomerMiddleware::class);

//会员授权接口
Route::group(function () {

    Route::get('logout', 'AuthController/logout')->name('logout');// 退出登录
    Route::post('binding', 'AuthController/binding_phone')->name('bindingPhone');// 绑定手机号

     //公共类
    Route::post('upload/image', 'PublicController/upload_image')->name('uploadImage');//图片上传

    //用户类  用户order
    Route::get('user', 'user.UserController/user')->name('user');//个人中心
    Route::post('user/edit', 'user.UserController/edit')->name('userEdit');//用户修改信息
    Route::get('user/balance', 'user.UserController/balance')->name('userBalance');//用户资金统计
    Route::get('userinfo', 'user.UserController/userinfo')->name('userinfo');// 用户信息
    //用户类  地址
    Route::get('address/detail/:id', 'user.UserController/address')->name('address');//获取单个地址
    Route::get('address/list', 'user.UserController/address_list')->name('addressList');//地址列表
    Route::post('address/default/set', 'user.UserController/address_default_set')->name('addressDefaultSet');//设置默认地址
    Route::get('address/default', 'user.UserController/address_default')->name('addressDefault');//获取默认地址
    Route::post('address/edit', 'user.UserController/address_edit')->name('addressEdit');//修改 添加 地址
    Route::post('address/del', 'user.UserController/address_del')->name('addressDel');//删除地址
    //用户类 收藏
    Route::get('collect/user', 'user.UserController/collect_user')->name('collectUser');//收藏产品列表
    Route::post('collect/add', 'user.UserController/collect_add')->name('collectAdd');//添加收藏
    Route::post('collect/del', 'user.UserController/collect_del')->name('collectDel');//取消收藏
    Route::post('collect/all', 'user.UserController/collect_all')->name('collectAll');//批量添加收藏

    //用戶类 分享
    Route::post('user/share', 'PublicController/user_share')->name('user_share');//记录用户分享
    //用户类 点赞
//    Route::post('like/add', 'user.UserController/like_add')->name('likeAdd');//添加点赞
//    Route::post('like/del', 'user.UserController/like_del')->name('likeDel');//取消点赞
    //用户类 关注
    Route::get('follows', 'user.UserAttentController/getfollow')->name('follows');//关注列表
    Route::get('fans', 'user.UserAttentController/getfans')->name('fans');//粉丝列表
    Route::post('attent/add', 'user.UserAttentController/add')->name('attent_add');//关注
    Route::post('attent/del', 'user.UserAttentController/del')->name('attent_del');//取消关注

    //用户类 签到
    Route::get('sign/config', 'user.UserController/sign_config')->name('signConfig');//签到配置
    Route::get('sign/list', 'user.UserController/sign_list')->name('signList');//签到列表
    Route::get('sign/month', 'user.UserController/sign_month')->name('signIntegral');//签到列表（年月）
    Route::post('sign/user', 'user.UserController/sign_user')->name('signUser');//签到用户信息
    Route::post('sign/integral', 'user.UserController/sign_integral')->name('signIntegral');//签到

    //购物车类
    Route::get('cart/list', 'store.StoreCartController/lst')->name('cartList'); //购物车列表
    Route::post('cart/add', 'store.StoreCartController/add')->name('cartAdd'); //购物车添加
    Route::post('cart/del', 'store.StoreCartController/del')->name('cartDel'); //购物车删除
    Route::post('order/cancel', 'order.StoreOrderController/cancel')->name('orderCancel'); //订单取消
    Route::post('cart/num', 'store.StoreCartController/num')->name('cartNum'); //购物车 修改产品数量
    Route::get('cart/count', 'store.StoreCartController/count')->name('cartCount'); //购物车 获取数量
    //订单类
    Route::post('order/confirm', 'order.StoreOrderController/confirm')->name('orderConfirm'); //订单确认
    Route::post('order/computed/:key', 'order.StoreOrderController/computedOrder')->name('computedOrder'); //计算订单金额
    Route::post('order/create/:key', 'order.StoreOrderController/create')->name('orderCreate'); //订单创建
    Route::get('order/data', 'order.StoreOrderController/data')->name('orderData'); //订单统计数据
    Route::get('order/list', 'order.StoreOrderController/lst')->name('orderList'); //订单列表
    Route::get('order/detail/:uni', 'order.StoreOrderController/detail')->name('orderDetail'); //订单详情
    Route::get('order/refund/reason', 'order.StoreOrderController/refund_reason')->name('orderRefundReason'); //订单退款理由
    Route::post('order/refund/verify', 'order.StoreOrderController/refund_verify')->name('orderRefundVerify'); //订单退款审核
    Route::post('order/take', 'order.StoreOrderController/take')->name('orderTake'); //订单收货
    Route::get('order/express/:uni', 'order.StoreOrderController/express')->name('orderExpress'); //订单查看物流
    Route::post('order/del', 'order.StoreOrderController/del')->name('orderDel'); //订单删除
    Route::post('order/again', 'order.StoreOrderController/again')->name('orderAgain'); //订单 再次下单
    Route::post('order/pay', 'order.StoreOrderController/pay')->name('orderPay'); //订单支付
    Route::post('order/product', 'order.StoreOrderController/product')->name('orderProduct'); //订单产品信息
    Route::post('order/comment', 'order.StoreOrderController/comment')->name('orderComment'); //订单评价

    //账单类
    Route::get('integral/list', 'user.UserBillController/integral_list')->name('integralList');//积分记录
    //提现类
    Route::get('extract/bank', 'user.UserExtractController/bank')->name('extractBank');//提现银行/提现最低金额
    Route::post('extract/cash', 'user.UserExtractController/cash')->name('extractCash');//提现申请
    //充值类
    Route::post('recharge/wechat', 'user.UserRechargeController/wechat')->name('rechargeWechat');//公众号充值
    Route::get('recharge/index','user.UserRechargeController/index')->name('rechargeQuota');//充值余额选择
    //会员等级类
    Route::get('menu/user', 'PublicController/menu_user')->name('menuUser');//个人中心菜单
    //首页获取未支付订单
    Route::get('order/nopay', 'order.StoreOrderController/get_noPay')->name('getNoPay');//获取未支付订单

    //直播
    Route::get('live/config', 'live.LiveController/config')->name('liveStart');//直播配置
	 Route::get('live/sa', 'live.LiveController/sa')->name('sa');//直播配置
    Route::get('live/class', 'live.LiveController/liveclass')->name('liveClass');//直播分类
    Route::post('live/start', 'live.LiveController/start')->name('liveStart');//开播
    Route::post('live/upLive', 'live.LiveController/upLive')->name('liveupLive');//开播
    Route::post('live/stop', 'live.LiveController/stop')->name('liveStop');//关播
    Route::get('live/stopinfo', 'live.LiveController/stopInfo')->name('stopInfo');//关播信息
    Route::post('live/check', 'live.LiveController/check')->name('livecheck');//房间检测 
    Route::post('live/enter', 'live.LiveController/enter')->name('liveenter');//进入房间
    Route::get('livenums', 'live.LiveController/getnums')->name('livenums');//直播间人数
    Route::get('livelikes', 'live.LiveController/getlikes')->name('livelikes');//直播间点赞数
    Route::post('livesetlike', 'live.LiveController/setlike')->name('livesetlike');//点赞
    Route::get('livegoodsnums', 'live.LiveController/getgoodsnums')->name('livegoodsnums');//商品数
    Route::get('livesearch', 'live.LiveController/search')->name('livesearch');//搜索列表
    Route::get('checklive', 'live.LiveController/checklive')->name('checklive');//搜索列表
    Route::get('livefollow', 'live.LiveController/follow')->name('livefollow');//关注直播


    //直播用户管理 
    Route::get('pop', 'live.LiveManageController/getpop')->name('livepop');//弹窗
    Route::post('livekick', 'live.LiveManageController/kick')->name('livekick');//踢出
    Route::post('liveshut', 'live.LiveManageController/shut')->name('liveshut');//禁言
    Route::post('liveunshut', 'live.LiveManageController/unshut')->name('liveunshut');//取消禁言
	
    

    //直播举报
    Route::get('livereportcat', 'live.LiveReportController/reportcat')->name('livereportcat');//举报类型
    Route::post('livereport', 'live.LiveReportController/report')->name('livereport');//举报

    //店铺
    Route::get('shop', 'shop.ShopController/myshop')->name('myShop');//我的店铺
    Route::get('shopstatus', 'shop.ShopController/applystatus')->name('shopStatus');//店铺状态
    Route::post('shopapply', 'shop.ShopController/apply')->name('shopApply');//店铺申请

    //提现账号 
    Route::get('account', 'user.UserAccountController/lst')->name('shopaddlist');//列表
    Route::post('accountadd', 'user.UserAccountController/add')->name('accountadd');//添加
    Route::post('accountedit', 'user.UserAccountController/edit')->name('accountedit');//编辑
    Route::post('accountdel', 'user.UserAccountController/del')->name('accountdel');//删除

    //店铺提现
    Route::get('shopcash', 'cash.ShopController/info')->name('shopcash');//店铺收益
    Route::get('shopcashlist', 'cash.ShopController/lst')->name('shopcashlist');//店铺提现记录
    Route::post('cashshop', 'cash.ShopController/cash')->name('cashshop');//店铺提现 
    Route::get('shopsettle', 'cash.ShopController/settlelst')->name('shopsettle');//店铺结算记录
	//新增接口
	Route::post('setmanager', 'live.LiveManageController/setmanager')->name('setmanager');//设置管理员
	Route::post('delmanager', 'live.LiveManageController/delmanager')->name('delmanager');//取消管理员
	Route::post('usershutlist', 'live.LiveManageController/usershutlist')->name('usershutlist');//禁言用户列表
    Route::post('liveunkick', 'live.LiveManageController/unkick')->name('liveunkick');//取消踢出	
    Route::post('userkicklist', 'live.LiveManageController/userkicklist')->name('liveunkick');//踢出列表	
    Route::post('supershut', 'live.LiveManageController/supershut')->name('supershut');//超管关播	
	Route::post('managelist', 'live.LiveManageController/managelist')->name('managelist');//管理房间列表
    Route::post('managerlist', 'live.LiveManageController/managerlist')->name('managerlist');//管理员列表

	
	Route::get('giftlist', 'live.GiftController/giftlist')->name('giftlist');//礼物列表
	Route::post('sendgift', 'live.GiftController/sendgift')->name('sendgift');//送礼物
	
	Route::get('getuserlist', 'live.LiveController/getuserlist')->name('getuserlist');//用户列表
    Route::post('checkshutup', 'live.LiveController/checkshutup')->name('checkshutup');//检测是否被禁言

    //礼物提现
    Route::get('votes', 'cash.GiftvotesController/info')->name('votes');//收益
    Route::get('voteslist', 'cash.GiftvotesController/lst')->name('voteslist');//提现记录
    Route::post('cashvotes', 'cash.GiftvotesController/cash')->name('cashvotes');//提现
    //充值类
	Route::post('getinfo', 'charge.ChargeController/getinfo')->name('getinfo');//充值信息
	Route::post('getorderbywx', 'charge.ChargeController/getorderbywx')->name('getorderbywx');//微信充值	
	Route::post('getorderbyali', 'charge.ChargeController/getorderbyali')->name('getorderbyali');//支付宝充值	
	

})->middleware(\app\http\middleware\AllowOriginMiddleware::class)->middleware(\app\http\middleware\AuthTokenMiddleware::class, true); 
//未授权接口 
Route::group(function () {
    //公共类
    Route::get('index', 'PublicController/index')->name('index');//首页
    Route::get('search/keyword', 'PublicController/search')->name('searchKeyword');//热门搜索关键字获取
    //产品分类类
    Route::get('category', 'store.CategoryController/category')->name('category');
    //产品类 
    Route::post('image_base64', 'PublicController/get_image_base64')->name('getImageBase64');// 获取图片base64
    Route::get('product/detail/:id/[:type]', 'store.StoreProductController/detail')->name('detail');//产品详情
    Route::get('groom/list/:type', 'store.StoreProductController/groom_list')->name('groomList');//获取首页推荐不同类型产品的轮播图和产品
    Route::get('products', 'store.StoreProductController/lst')->name('products');//产品列表
    Route::get('product/hot', 'store.StoreProductController/product_hot')->name('productHot');//为你推荐
    Route::get('reply/list/:id', 'store.StoreProductController/reply_list')->name('replyList');//产品评价列表
    Route::get('reply/config/:id', 'store.StoreProductController/reply_config')->name('replyConfig');//产品评价数量和好评度

    //文章分类类
    Route::get('article/category/list', 'publics.ArticleCategoryController/lst')->name('articleCategoryList');//文章分类列表
    //文章类
    Route::get('article/list/:cid', 'publics.ArticleController/lst')->name('articleList');//文章列表
    Route::get('article/details/:id', 'publics.ArticleController/details')->name('articleDetails');//文章详情
    Route::get('article/hot/list', 'publics.ArticleController/hot')->name('articleHotList');//文章 热门
    Route::get('article/banner/list', 'publics.ArticleController/banner')->name('articleBannerList');//文章 banner

    //APP 
    Route::post('thirdlogin', 'wechat.AuthController/third_auth')->name('thirdlogin');//APP三方登录

    //物流公司
    Route::get('logistics', 'PublicController/logistics')->name('logistics');//物流公司列表

    //短信购买异步通知
    Route::post('sms/pay/notify', 'PublicController/sms_pay_notify')->name('smsPayNotify'); //短信购买异步通知

    //获取城市列表
    Route::get('city_list', 'PublicController/city_list')->name('cityList');

    //精选
    Route::get('featured', 'live.LiveController/featured')->name('featuredData');
    //分类下直播
    Route::get('classlive/:classid', 'live.LiveController/classlive')->name('classLive');

    //获取公共配置 
    Route::get('config', 'PublicController/getconfig')->name('getconfig');
    Route::get('test', 'PublicController/test')->name('test');

    Route::get('liveinfo/:liveuid', 'live.LiveController/getinfo')->name('liveinfo');//主播直播信息 

    Route::get('shopservice', 'PublicController/getservice')->name('shopService');//店铺客服
	
	//新增接口

	Route::get('shutlist', 'live.LiveManageController/shutlist')->name('shutlist');//禁言选择时间列表
	

})->middleware(\app\http\middleware\AllowOriginMiddleware::class)->middleware(\app\http\middleware\AuthTokenMiddleware::class, false);


Route::miss(function() {
    if(app()->request->isOptions())
        return \think\Response::create('ok')->code(200)->header([
            'Access-Control-Allow-Origin'   => '*',
            'Access-Control-Allow-Headers'  => 'Authori-zation,Authorization, Content-Type, If-Match, If-Modified-Since, If-None-Match, If-Unmodified-Since, X-Requested-With',
            'Access-Control-Allow-Methods'  => 'GET,POST,PATCH,PUT,DELETE,OPTIONS,DELETE',
        ]);
    else
        return \think\Response::create()->code(404);
});